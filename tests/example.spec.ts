import { test, expect } from "@playwright/test";

test.use({ 
  locale: 'en-US',
  geolocation:{longitude: 2.2990125,latitude: 48.8598277},
  permissions:['geolocation']
});

test.describe("two tests", () => {
  test("basic test", async ({ page }) => {
    await page.goto("https://playwright.dev/");
    await page.locator("text=Get started").click();
    await expect(page).toHaveTitle(/Getting started/);
  });
});
